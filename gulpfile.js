'use strict';

var gulp = require('gulp'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create(),
    cssmin = require('gulp-cssmin'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    wiredep = require('wiredep').stream,
    uncache = require('gulp-uncache');


var reload = browserSync.reload;

gulp.task('default', ['watch', 'bower', 'sass'], function() {
    browserSync.init({
        notify: false,
        port: 9000,
        server: {
            baseDir: [
                'app'
            ]
        }
    });
});

gulp.task('watch', function() {
    gulp.watch([
        'app/index.html',
        'app/templates/**/*.html',
        'app/templates/**/**/*.html',
        'app/components/**/*.html',
        'app/app.js',
        'app/templates/**/*.js',
        'app/templates/**/**/*.js',
        'app/components/**/*.js',
        'app/models/*.js',
        'app/factory/*.js',
        'app/filters/*.js',
        'app/resolves*.js'
    ]).on('change', reload);
    gulp.watch('bower.json', ['bower']);
    gulp.watch([
            'app/style.scss',
            'app/_settings.scss',
            'app/_helpers.scss',
            'app/templates/**/*.scss',
            'app/templates/**/**/*.scss',
            'app/components/**/*.scss'
        ], ['sass']
    );

});

gulp.task('fonts', function() {
    return gulp.src([
        'app/fonts/**/*.*'])
        .pipe(gulp.dest('dist/css/fonts/'));
});

gulp.task('bower-fonts', function() {
    return gulp.src(['app/bower_components/ionicons/fonts/*.*'])
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('sass', function() {
    return gulp.src('app/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app'))
        .pipe(reload({stream: true}));
});

gulp.task('bower', function() {
    gulp.src('app/index.html')
        .pipe(wiredep({
            directory: "app/bower_components"
        }))
        .pipe(gulp.dest('./app'));
});

gulp.task('html', function() {
    return gulp.src(['app/**/*.html', '!app/bower_components/**/*.html'])
        .pipe(gulp.dest('dist/'));
});

gulp.task('images', function() {
    return gulp.src(['app/images/*.gif', 'app/images/*.jpg', 'app/images/*.png', 'app/images/*.svg'])
        .pipe(gulp.dest('dist/images/'));
});

gulp.task('build', ['bower', 'html', 'images', 'fonts', 'bower-fonts'], function() {
    return gulp.src('app/index.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', cssmin()))
        .pipe(uncache())
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', function() {
    return gulp.src(['dist'])
        .pipe(clean());
});