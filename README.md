# Cosmonauts test project


## Build Setup

``` bash
# install dependencies
npm install
bower install

# run server
gulp default

# build dist folder
gulp build

# clean
gulp clean
```