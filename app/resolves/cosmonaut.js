var resolveCosmonaut = [
    '$stateParams', '$localStorage', '$http', '$state', 'url', 'toast',
    function (
        $stateParams, $localStorage, $http, $state, url, toast
    ) {
        return $http({
            dataType: 'json',
            method: 'GET',
            url: url.cosmonauts.get+'/'+$stateParams.id,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Authorization': $localStorage.auth_token
            }
        }).then(function(response){
            return response.data;
        }, function () {
            toast('error', 'Nelze najít kosmonauta.');
            $state.go('user.cosmonauts.list');
            return null;
        });
    }];