angular.module('factory.toast', [])
    .factory('toast', [
        '$mdToast',
        function(
            $mdToast
        ) {
            return function(type, text, delay) {
                if(text && text.length > 0) {
                    var timeout = delay || (text.trim().length * 200);

                    $mdToast.show({
                        hideDelay   : timeout,
                        position    : 'top right',
                        controller  : [
                            '$scope', '$mdToast', '$interval', 'text', 'type',
                            function($scope, $mdToast, $interval, text, type) {
                                $scope.text = text;
                                $scope.type = type;

                                $scope.determinateValue = 0;
                                $interval(function() {
                                    $scope.determinateValue += 1;
                                }, timeout/100, 0, true);

                                $scope.closeToast = function() {
                                    $mdToast.hide().then(function(){});
                                };
                            }],
                        templateUrl : 'templates/toast/toast_tpl.html',
                        locals: {
                            text: text,
                            type: type
                        }
                    });
                }

            }
        }
    ]);


