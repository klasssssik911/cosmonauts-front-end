angular.module('factory.request', [
    'factory.url',
    'ngStorage'
])
    .factory('api', [
        '$http',
        'toast',
        '$localStorage',
        '$state',
        '$log',
        function(
            $http,
            toast,
            $localStorage,
            $state,
            $log) {

            /**
             * Set request config
             * @param {string} method - request method (GET | POST | PUT | DELETE)
             * @param {string} url - request url
             * @param {object} data - request data
             * @param {function} successCallback
             * @param {function} errorCallback
             * @returns {*} - promise response
             */
            var request = function(method, url, data, successCallback, errorCallback) {
                var config = {
                    dataType: 'json',
                    method: method,
                    url: url,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                };

                if($localStorage.auth_token) config.headers['Authorization'] = $localStorage.auth_token;
                method == "GET" ? config.params = data : config.data = data;

                return $http(config).then(
                    function(response) {
                        $log.debug('response', url, response);
                        if (response.data.message && response.data.error) {
                            toast('error', response.data.message);
                        }
                        successCallback(response.data);
                    },
                    function(response) {
                        $log.debug('error', url, response);
                        if (response.status == 200) {
                            toast('error', "Server Error: " + response.data);
                        } else if (response.status == -1) {
                            toast('error', "Server unavailable");
                        } else if (response.status == 500) {
                            toast('error', "Server Error: " + response.status + ' ' + response.data.message);
                        } else {
                            toast('error', "Server Error: " + response.status + ' ' + response.data && response.data.message ? response.data.message : response.statusText);
                        }
                        if(errorCallback) errorCallback();
                    }
                )
            };

            return {
                /**
                 * Get request (get data)
                 * @param {string} url - request url
                 * @param {object} data - request data
                 * @param {function=} successCallback
                 * @param {function=} errorCallback
                 * @returns {*} - promise request
                 */
                get: function(url, data, successCallback, errorCallback) {
                    return request('GET', url, data, successCallback, errorCallback);
                },

                /**
                 * Post request (create data)
                 * @param {string} url - request url
                 * @param {object} data - request data
                 * @param {function=} successCallback
                 * @param {function=} errorCallback
                 * @returns {*} - promise request
                 */
                post: function(url, data, successCallback, errorCallback) {
                    return request('POST', url, data, successCallback, errorCallback);
                },

                /**
                 * Delete request (delete data)
                 * @param {string} url - request url
                 * @param {object} data - request data
                 * @param {function=} successCallback
                 * @param {function=} errorCallback
                 * @returns {*} - promise request
                 */
                delete: function(url, data, successCallback, errorCallback) {
                    return request('DELETE', url, data, successCallback, errorCallback);
                },

                /**
                 * Put request (change data)
                 * @param {string} url - request url
                 * @param {object} data - request data
                 * @param {function=} successCallback
                 * @param {function=} errorCallback
                 * @returns {*} - promise request
                 */
                put: function(url, data, successCallback, errorCallback) {
                    return request('PUT', url, data, successCallback, errorCallback);
                }
            }
        }
    ]);