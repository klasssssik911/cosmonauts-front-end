angular.module('factory.url', [])
    .factory('url', ['$location',
        function($location) {
            var baseUrl = "http://api.denys-sobko-cosmonauts.tk:8080/";
            var cosmonautsPath = baseUrl + 'cosmonauts';
            return {
                baseUrl: baseUrl,
                user: {
                    signUp: baseUrl + 'signup',                     // POST method
                    login: baseUrl + 'auth/login',                  // POST method
                    current: baseUrl + 'current',                   // GET method
                    update: baseUrl + 'user',                       // PUT method
                    updatePassword: baseUrl + 'password'            // PUT method
                },
                cosmonauts: {
                    all: cosmonautsPath,                           // GET method
                    deleteList: cosmonautsPath + '/destroy-list',  // DELETE method
                    create: cosmonautsPath,                        // POST method
                    update: cosmonautsPath,                        // PUT method
                    get: cosmonautsPath,                           // GET method
                    delete: cosmonautsPath                         // DELETE method
                }
            }
        }
    ]);