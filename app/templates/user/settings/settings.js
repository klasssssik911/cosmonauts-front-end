app.controller('settingsCtrl', [
    '$scope',
    '$element',
    '$rootScope',
    '$state',
    '$mdDialog',
    'toast',
    'userModel',
    function($scope,
             $element,
             $rootScope,
             $state,
             $mdDialog,
             toast,
             userModel) {

        /**
         * User data
         * @type {object} userData
         * @type {string} userData.name - user full name
         */
        $scope.userData = {};

        /**
         * Password data
         * @type {object} passwordData
         * @type {string} passwordData.current - current password
         * @type {string} passwordData.new_password - new password
         * @type {string} passwordData.password_confirmation - new password confirmation
         */
        $scope.passwordData = {};

        /**
         * Use for show/hide progress lines
         * @type {{data: boolean, password: boolean}}
         */
        $scope.isLoading = {
            data: false,
            password: false
        };

        /**
         * Initialize user date for editing personal information (name)
         */
        $scope.initUserData = function () {
            $scope.userData = angular.copy($rootScope.user);
        };

        /**
         * Update user data
         * @param {object} userData
         * @param {string} userData.name - Full name of current user
         */
        $scope.updateUserData = function (userData) {
            $scope.isLoading.data = true;
            userModel.update({data: userData}, function(data) {
                $scope.isLoading.data = false;
                toast('success', 'Změny byly úspěšně uloženy.');
                $rootScope.user = data.user;
                $scope.initUserData();
                $scope.$$childHead.userForm.$setPristine(true);
            }, function () {
                $scope.isLoading.data = false;
            });
        };

        /**
         * Update user password
         * @param {object} passwordData
         * @param {object} passwordData.current - user current password
         * @param {object} passwordData.new_password - user new password
         * @param {object} passwordData.password_confirmation - confirmation of new password
         */
        $scope.updatePassword = function (passwordData) {
            $scope.isLoading.password = true;
            userModel.updatePassword({data: passwordData}, function(data) {
                $scope.isLoading.password = false;
                if(data.success){
                    toast('success', 'Heslo úspěšně změněno.');
                    for(var name in passwordData){
                        $scope.$$childHead.passwordForm[name].$touched = false;
                    }
                    $scope.passwordData = {};
                    $scope.$$childHead.passwordForm.$setPristine();
                }
            }, function () {
                $scope.isLoading.password = false;
            });
        };
    }
]);