app.controller('userCtrl', [
        '$scope',
        '$rootScope',
        '$state',
        '$timeout',
        '$mdSidenav',
        '$log',
        function($scope,
                 $rootScope,
                 $state,
                 $timeout,
                 $mdSidenav,
                 $log) {

            /**
             * Menu buttons
             * @type {*[]}
             */
            $scope.menu = [
                {
                    title: 'Kosmonauti',
                    icon: 'ion-ios-people',
                    state: 'user.cosmonauts.list',
                    states: ['user.cosmonauts.list', 'user.cosmonauts.show']
                },{
                    title: 'Nastavení',
                    icon: 'ion-settings',
                    state: 'user.settings',
                    states: ['user.settings']
                }
            ];

            /**
             * Check active button in left menu
             * @param button - button in left menu
             * @returns {boolean}
             */
            $scope.isCurrentState = function (button) {
                if($rootScope.$state){
                    return button.states.indexOf($rootScope.$state.current.name) != -1;
                }else{
                    return false;
                }
            };
            
            $scope.showMenu = buildDelayedToggler('menu');

            /**
             * Supplies a function that will continue to operate until the
             * time is up.
             */
            function debounce(func, wait, context) {
                var timer;
                return function debounced() {
                    var context = $scope,
                        args = Array.prototype.slice.call(arguments);
                    $timeout.cancel(timer);
                    timer = $timeout(function() {
                        timer = undefined;
                        func.apply(context, args);
                    }, wait || 10);
                };
            }

            /**
             * Build handler to open/close a SideNav; when animation finishes
             * report completion in console
             */
            function buildDelayedToggler(navID) {
                return debounce(function() {
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {
                            $log.debug("toggle " + navID + " is done");
                        });
                }, 200);
            }

            /**
             * Close left menu
             */
            $scope.closeMenu = function () {
                $mdSidenav('menu').close()
                    .then(function () {
                        $log.debug("close menu is done");
                    });

            };

            /**
             * Open left menu
             */
            $scope.$on('openLeftMenu',function () {
                $scope.showMenu();
            });
        }
    ]);