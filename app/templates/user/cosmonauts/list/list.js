app.controller('cosmonautsListCtrl', [
    '$scope',
    '$localStorage',
    '$state',
    '$mdDialog',
    '$http',
    'toast',
    'url',
    function($scope,
             $localStorage,
             $state,
             $mdDialog,
             $http,
             toast,
             url) {

        /**
         * Cosmonauts array and total count for table
         * @type {{count: number, data: Array}}
         */
        $scope.cosmonauts = {
            count: 0,
            data: []
        };

        /**
         * Pagination per page
         * @type {number[]}
         */
        $scope.paginationOptions = [10, 20, 50];

        /**
         * Query data
         * @type {{order: string, search_text: null, limit: number, page: number}}
         */
        $scope.query = {
            order: '-created_at',
            search_text: null,
            limit: $scope.paginationOptions[0],
            page: 1
        };

        /**
         * Creating new Cosmonaut
         * @param event - button event
         */
        $scope.addCosmonaut = function (event) {
            $mdDialog.show({
                controller: cosmonautFormController,
                templateUrl: 'components/cosmonautForm/cosmonautForm.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:false,
                fullscreen: false,
                locals: {
                    mode: 'create',
                    cosmonaut: null
                }
            }).then(function(data) {
                // update list after adding new cosmonaut
                $scope.getCosmonauts();
            }, function() {});
        };

        /**
         * Open cosmonaut details page
         * @param id - cosmonaut id
         */
        $scope.openCosmonaut = function(id) {
            $state.go('user.cosmonauts.show',{id: id});
        };

        /**
         * Get list of cosmonauts with total count
         */
        $scope.getCosmonauts = function () {
            $scope.promise = $http({
                dataType: 'json',
                method: 'GET',
                url: url.cosmonauts.all,
                params: $scope.query,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': $localStorage.auth_token
                }
            }).then(function(response){
                $scope.cosmonauts.data = response.data.items;
                $scope.cosmonauts.count = response.data.total;
            });
        };

        /**
         * Load cosmonauts
         */
        $scope.getCosmonauts();
    }
]);