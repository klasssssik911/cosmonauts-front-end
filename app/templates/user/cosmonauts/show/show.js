app.controller('cosmonautDetailCtrl', [
    '$scope',
    '$state',
    '$mdDialog',
    'toast',
    'url',
    'cosmonautsModel',
    'cosmonaut',
    function($scope,
             $state,
             $mdDialog,
             toast,
             url,
             cosmonautsModel,
             cosmonaut) {

        /**
         * Use for show/hide progress line
         * @type {boolean}
         */
        $scope.isLoading = false;

        /**
         * Opened cosmonaut
         * @type {object}
         */
        $scope.cosmonaut = cosmonaut;

        /**
         * Editing Cosmonaut
         * @param event - button event
         */
        $scope.editCosmonaut = function (event) {
            $mdDialog.show({
                controller: cosmonautFormController,
                templateUrl: 'components/cosmonautForm/cosmonautForm.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:false,
                fullscreen: false,
                locals: {
                    mode: 'edit',
                    cosmonaut: $scope.cosmonaut
                }
            }).then(function(updatedCosmonaut) {
                // update cosmonaut data
                $scope.cosmonaut = updatedCosmonaut;
            }, function() {});
        };

        /**
         * Delete Cosmonaut
         * @param event - button event
         */
        $scope.deleteCosmonaut = function (event) {
            var confirm = $mdDialog.confirm()
                .title('Chtěli byste vymazat kosmonauta?')
                .textContent('Tyto údaje budou trvale odstraněne.')
                .ariaLabel('Vymazat kosmonauta')
                .targetEvent(event)
                .ok('Prosím udělej to!')
                .cancel('Nechci');
            $mdDialog.show(confirm).then(function() {
                // show progress line
                $scope.isLoading = true;
                // delete cosmonaut
                cosmonautsModel.delete({id: $scope.cosmonaut.id}, function(data) {
                    // hide progress line
                    $scope.isLoading = false;
                    if(data.success){
                        // show succes message
                        toast('success', 'Kosmonaut uspešně vymazan.');
                        // redirect to the list of cosmonauts
                        $state.go('user.cosmonauts.list');
                    }
                }, function () {
                    // hide progress line
                    $scope.isLoading = false;
                });
            }, function() {
                // cancel answer
            });
        };
    }
]);