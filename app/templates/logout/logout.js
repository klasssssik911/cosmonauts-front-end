angular.module('cosmonauts')
    .controller('logoutCtrl', [
        '$scope',
        '$rootScope',
        '$localStorage',
        '$state',
        function($scope,
                 $rootScope,
                 $localStorage,
                 $state) {

            // delete auth_token from $localStorage
            if($localStorage.auth_token) delete $localStorage.auth_token;

            // delete user data from $rootScope
            if($rootScope.user) delete $rootScope.user;

            // redirect to the Home page
            $state.go('home');
        }
    ]);