angular.module('cosmonauts')
    .controller('homeCtrl', [
        '$scope',
        '$element',
        '$timeout',
        '$window',
        '$rootScope',
        '$localStorage',
        '$state',
        '$mdDialog',
        'toast',
        'userModel',
        function($scope,
                 $element,
                 $timeout,
                 $window,
                 $rootScope,
                 $localStorage,
                 $state,
                 $mdDialog,
                 toast,
                 userModel) {

            /**
             * Variable for Login progress line
             * @type {boolean}
             */
            $scope.isLoginLoading = false;

            /**
             * Variable for login object
             * @type {object}
             */
            $scope.loginData = {};

            /**
             * Access to localStorage from home.html
             * @type {$localStorage|*}
             */
            $scope.storage = $localStorage;

            $timeout(function () {
                initParticles();
            },0);

            /**
             * Initialize particles on the home page
             */
            function initParticles(){
                var particlesConfig = {
                    "particles": {
                        "number": {
                            "value": 160,
                            "density": {
                                "enable": true,
                                "value_area": 800
                            }
                        },
                        "color": {
                            "value": "#ffffff"
                        },
                        "shape": {
                            "type": "circle",
                            "stroke": {
                                "width": 0,
                                "color": "#000000"
                            },
                            "polygon": {
                                "nb_sides": 5
                            },
                            "image": {
                                "src": "img/github.svg",
                                "width": 100,
                                "height": 100
                            }
                        },
                        "opacity": {
                            "value": 1,
                            "random": true,
                            "anim": {
                                "enable": true,
                                "speed": 1,
                                "opacity_min": 0,
                                "sync": false
                            }
                        },
                        "size": {
                            "value": 3,
                            "random": true,
                            "anim": {
                                "enable": false,
                                "speed": 4,
                                "size_min": 0.3,
                                "sync": false
                            }
                        },
                        "line_linked": {
                            "enable": false,
                            "distance": 150,
                            "color": "#ffffff",
                            "opacity": 0.4,
                            "width": 1
                        },
                        "move": {
                            "enable": true,
                            "speed": 1,
                            "direction": "none",
                            "random": true,
                            "straight": false,
                            "out_mode": "out",
                            "bounce": false,
                            "attract": {
                                "enable": false,
                                "rotateX": 600,
                                "rotateY": 600
                            }
                        }
                    },
                    "interactivity": {
                        "detect_on": "canvas",
                        "events": {
                            "onhover": {
                                "enable": true,
                                "mode": "bubble"
                            },
                            "onclick": {
                                "enable": true,
                                "mode": "repulse"
                            },
                            "resize": true
                        },
                        "modes": {
                            "grab": {
                                "distance": 400,
                                "line_linked": {
                                    "opacity": 1
                                }
                            },
                            "bubble": {
                                "distance": 250,
                                "size": 0,
                                "duration": 2,
                                "opacity": 0,
                                "speed": 3
                            },
                            "repulse": {
                                "distance": 400,
                                "duration": 0.4
                            },
                            "push": {
                                "particles_nb": 4
                            },
                            "remove": {
                                "particles_nb": 2
                            }
                        }
                    },
                    "retina_detect": true
                };
                particlesJS('particles-js', particlesConfig);
            }

            /**
             * Login function
             * @param {object} loginData
             * @param {string} loginData.email - User email
             * @param {string} loginData.password - User password
             */
            $scope.login = function (loginData) {
                // show progress line
                $scope.isLoginLoading = true;
                userModel.login({auth: loginData}, function(data) {
                    // if user data exist
                    if(data.user){
                        // save user date to the $rootScope
                        $rootScope.user = data.user;
                        // and redirect to user profile
                        $state.go('user.profile');
                    }
                    // hide progress line
                    $scope.isLoginLoading = false;
                }, function () {
                    // hide progress line
                    $scope.isLoginLoading = false;
                });
            };

            /**
             * Function for open registration user modal window
             * @param event - button event
             */
            $scope.signUp = function (event) {
                $mdDialog.show({
                    controller: signUpController,
                    templateUrl: 'components/signUp/signUp.html',
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: false,
                    fullscreen: false
                }).then(function() {
                }, function() {
                });
            };
        }
    ]);