// http-server -a localhost -p 8000 -c-1

var app = angular.module('cosmonauts', [
    'ngMaterial',
    'ngMessages',
    'ui.router',
    'md.data.table',
    'ngSanitize',
    'ngStorage',
    'model.user',
    'model.cosmonauts',
    'factory.request',
    'factory.toast',
    'cosmonauts.filters',
    'ui.mask'
]).config([
    '$locationProvider',
    '$stateProvider',
    '$urlRouterProvider',
    '$mdIconProvider',
    '$mdDateLocaleProvider',
    '$mdThemingProvider',
    function(
        $locationProvider,
        $stateProvider,
        $urlRouterProvider,
        $mdIconProvider,
        $mdDateLocaleProvider,
        $mdThemingProvider) {

        $mdDateLocaleProvider.formatDate = function(date) {
            return moment(date).format('YYYY-MM-DD');
        };

        // State provider
        $stateProvider
            // State for Home page
            .state('home', {
                url: "/",
                templateUrl: "templates/home/home.html",
                controller: "homeCtrl"
            })
            // Abstract state for User pages
            .state('user', {
                abstract: true,
                templateUrl: "templates/user/user.html",
                controller: "userCtrl"
            })
            // State for User Profile page
            .state('user.profile', {
                url: "/profile",
                controller: "profileCtrl",
                templateUrl: "templates/user/profile/profile.html"
            })
            // Abstract state for User Cosmonauts pages
            .state('user.cosmonauts', {
                abstract: true,
                url: "/cosmonauts",
                templateUrl: "templates/user/cosmonauts/cosmonauts.html",
                controller: "cosmonautsCtrl"
            })
            // State for User all Cosmonauts page
            .state('user.cosmonauts.list', {
                url: "/list",
                controller: "cosmonautsListCtrl",
                templateUrl: "templates/user/cosmonauts/list/list.html"
            })
            // State for viewing Cosmonauts details
            .state('user.cosmonauts.show', {
                url: "/:id",
                controller: "cosmonautDetailCtrl",
                templateUrl: "templates/user/cosmonauts/show/show.html",
                resolve: {
                    cosmonaut: resolveCosmonaut
                }
            })
            // State for User Settings
            .state('user.settings', {
                url: "/settings",
                controller: "settingsCtrl",
                templateUrl: "templates/user/settings/settings.html"
            })
            // State for Logout
            .state('logout', {
                url: "/logout",
                controller: "logoutCtrl"
            });

        $locationProvider.html5Mode(location.host.indexOf('localhost') == -1 && location.host.indexOf('127.0.0.1') == -1);

        // redirect to the root path if unknown path
        $urlRouterProvider.when('?', '/');
        // root path
        $urlRouterProvider.otherwise('/');

        var orangeMap = $mdThemingProvider.extendPalette('orange', {
            '50': 'fff8e7',
            '100': 'ffedbe',
            '200': 'ffe194',
            '300': 'fed262',
            '400': 'ffc42b',
            '500': 'fab70a',
            '600': 'fa9d0a',
            '700': 'e26920',
            '800': 'd55b11',
            '900': 'c44c04',
            'A100': 'a42a00',
            'A200': 'ff5252',
            'A400': 'ff1744',
            'A700': 'd50000',
            'contrastDefaultColor': 'dark'
        });
        $mdThemingProvider.definePalette('orange', orangeMap);
        $mdThemingProvider.theme('default').primaryPalette('orange');
    }
]).run(['$localStorage', '$location', '$rootScope', '$state', 'userModel',
    function($localStorage, $location, $rootScope, $state, userModel) {

        // access to $state from all views
        $rootScope.$state = $state;

        /**
         * Variable for store logged user data
         * @type {object} user
         * @type {object} user.id - logged user id
         * @type {object} user.name - logged user name
         */
        $rootScope.user = null;

        // load user if user is logged
        if ($localStorage.auth_token) {
            // get user data
            userModel.get(function(data) {
                // save user data
                $rootScope.user = data.user;
            },function () {
                // if we have error
                // then clear auth_token
                $localStorage.auth_token = null;
                // redirect to the Home page
                $state.go('home');
            });
        }

        // changing content
        $rootScope.$on('$viewContentLoading', function(event) {
            // current state name
            var stateName = $state.current.name;
            // if state name is not blank
            if(stateName && stateName.length > 0){
                // array of states that do not need auth_token
                var noLoggedPage = ['home'];
                // if current state does not exist in noLoggedPage array
                if (noLoggedPage.indexOf(stateName) == -1) {
                    // if auth_token and user does not exist
                    if (!$localStorage.auth_token && !$rootScope.user) {
                        // redirect to the home page
                        // for example: if guest will try to open profile page without valid auth_token in $localStorage
                        // and his data in $rootScope
                        event.preventDefault();
                        $state.go('home');
                    }
                }
            }
        });

    }]);