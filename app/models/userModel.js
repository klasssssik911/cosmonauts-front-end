angular.module('model.user', [
    'ngStorage'
])
    .service('userModel', ['url', 'api', 'toast', '$rootScope', '$localStorage',
        function(url, api, toast, $rootScope, $localStorage) {
            /**
             * Login user
             * @param {object} user
             * @param {string} user.email - user email
             * @param {string} user.password - user password
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.login = function(user, callback, errorCallback) {
                api.post( url.user.login, user,
                    function(data) {
                        $localStorage.auth_token = data.auth_token;
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Get current user by auth_token
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.get = function(callback, errorCallback) {
                api.get( url.user.current, {},
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Create new user
             * @param {object} data - registration data
             * @param {string} data.name - user full name
             * @param {string} data.email - user email
             * @param {string} data.password - user password
             * @param {string} data.password_confirmation - user password confirmation
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.signUp = function (data, callback, errorCallback) {
                api.post( url.user.signUp, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Update current user data
             * @param {object} data
             * @param {string} data.name - Full name of current user
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.update = function (data, callback, errorCallback) {
                api.put( url.user.update, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Update user password
             * @param {object} data
             * @param {string} data.current - current password
             * @param {string} data.new_password - new password
             * @param {string} data.password_confirmation - new password confirmation
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.updatePassword = function (data, callback, errorCallback) {
                api.put( url.user.updatePassword, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };
        }
    ]);