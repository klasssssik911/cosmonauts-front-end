angular.module('model.cosmonauts', [
    'ngStorage'
])
    .service('cosmonautsModel', ['url', 'api',
        function(url, api) {
            /**
             * Return user cosmonauts
             * @param {object} data
             * @param {number} data.page - page
             * @param {number} data.per_page - per page
             * @param {string} data.sort - sort by
             * @param {string} data.search_text - sort by text
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.all = function(data, callback, errorCallback) {
                api.get( url.cosmonauts.all, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Delete list of cosmonauts
             * @param {object} data
             * @param {array} data.ids - array of cosmonauts ids
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.deleteList = function(data, callback, errorCallback) {
                api.delete( url.cosmonauts.deleteList, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Delete cosmonaut by ID
             * @param {object} data
             * @param {array} data.id - cosmonaut id
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.delete = function(data, callback, errorCallback) {
                api.delete( url.cosmonauts.delete+'/'+data.id, {},
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Create new cosmonaut
             * @param data
             * @param {string} data.first_name - Cosmonaut first name
             * @param {string} data.last_name - Cosmonaut last name
             * @param {string} data.email - Cosmonaut email
             * @param {string} data.phone - Cosmonaut phone
             * @param {string} data.personal_number - Cosmonaut personal number
             * @param {number} data.gender - Cosmonaut gender
             * @param {string} data.dob - Cosmonaut date of birth
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.create = function(data, callback, errorCallback) {
                api.post( url.cosmonauts.create, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

            /**
             * Update cosmonaut
             * @param data
             * @param {number} data.id - Cosmonaut id
             * @param {string} data.first_name - Cosmonaut first name
             * @param {string} data.last_name - Cosmonaut last name
             * @param {string} data.email - Cosmonaut email
             * @param {string} data.phone - Cosmonaut phone
             * @param {string} data.personal_number - Cosmonaut personal number
             * @param {number} data.gender - Cosmonaut gender
             * @param {string} data.dob - Cosmonaut date of birth
             * @param {function} callback - request callback
             * @param {function} errorCallback - request error callback
             */
            this.update = function(data, callback, errorCallback) {
                api.put( url.cosmonauts.update+'/'+data.cosmonaut.id, data,
                    function(data) {
                        if(callback) callback(data);
                    }, function () {
                        if(errorCallback) errorCallback();
                    }
                );
            };

        }
    ]);