var signUpController = [
    '$scope',
    '$mdDialog',
    'userModel',
    'toast',
    function(
        $scope,
        $mdDialog,
        userModel,
        toast
    ) {

        /**
         * Variable for store all registration data
         * @type {object} signUpData
         * @type {object} signUpData.name - user full name
         * @type {object} signUpData.email - user email
         * @type {object} signUpData.password - user password
         * @type {object} signUpData.password_confirmation - user password confirmation
         */
        $scope.signUpData = {};

        /**
         * Variable for show/hide registration progress line
         * @type {boolean}
         */
        $scope.isSignUpLoading = false;

        /**
         * Registration new user
         * @param {object} signUpData
         */
        $scope.signUp = function (signUpData) {
            // show progress line
            $scope.isSignUpLoading = true;
            userModel.signUp(signUpData, function(data) {
                // hide progress line
                $scope.isSignUpLoading = false;
                // if auth_token present
                if(data.auth_token){
                    // show success message
                    toast('success', 'Úspěšně jste se zaregistrovali. Můžete se přihlásit do svého profilu.');
                    // close registration modal window
                    $scope.cancel();
                }else{
                    // show error message
                    toast('error', data.message);
                }
            }, function () {
                // hide progress line
                $scope.isSignUpLoading = false;
            });
        };

        /**
         * Close modal window
         */
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }];