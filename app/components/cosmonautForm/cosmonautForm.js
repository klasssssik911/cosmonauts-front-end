var cosmonautFormController = [
    '$scope',
    '$mdDialog',
    'cosmonautsModel',
    'toast',
    'mode',
    'cosmonaut',
    function(
        $scope,
        $mdDialog,
        cosmonautsModel,
        toast,
        mode,
        cosmonaut
    ) {

        /**
         * Max date for date time picker
         * @type {Date}
         */
        $scope.maxDate = new Date();

        /**
         * if cosmonaut data is present
         */
        if(cosmonaut){
            $scope.cosmonautData = angular.copy(cosmonaut);
            $scope.cosmonautData.gender = $scope.cosmonautData.gender == true ? 1 : 0;
        }else{
            $scope.cosmonautData = {
                superpowers: [
                    {
                        title: null
                    }
                ],
                dob: new Date()
            };
        }

        /**
         * Variable for progress line
         * @type {boolean}
         */
        $scope.isLoading = false;

        /**
         * Current form mode (create|edit)
         */
        $scope.mode = mode;

        /**
         * List of genders
         * @type {*[]}
         */
        $scope.genders = [
            {
                value: 1,
                title: 'Muž'
            },{
                value: 0,
                title: 'Žena'
            }
        ];

        /**
         * Add new Superpower
         */
        $scope.addSuperpower = function () {
            $scope.cosmonautData.superpowers.push({
                title: null
            });
        };

        /**
         * Delete cosmonaut Superpower by index
         * @param index - superpower index in array
         */
        $scope.deleteSuperpower = function (index) {
            if($scope.cosmonautData.superpowers.length > 1){
                $scope.cosmonautData.superpowers.splice(index, 1);
            }
        };

        /**
         * Check if Superpowers fields is valid
         * @param cosmonautForm - cosmonaut form
         * @returns {boolean}
         */
        $scope.isInvalidSuperpowers = function (cosmonautForm) {
            if(!cosmonautForm) return true;
            var isInvalid = false;
            for(var index in $scope.cosmonautData.superpowers){
                if(cosmonautForm['superpowers_'+index] && cosmonautForm['superpowers_'+index].$invalid){
                    isInvalid = true;
                    break;
                }
            }
            return isInvalid;
        };

        /**
         * Create/Update Cosmonaut error callback
         * @param data - response data
         */
        function successCallback(data){
            // hide progress line
            $scope.isLoading = false;
            // if id is present in response data
            if(data.id){
                // if current mode is 'create'
                if($scope.mode == 'create'){
                    // show success message about successful creating new cosmonaut
                    toast('success', 'Kosmonaut uspěšně přidán do vašeho seznamu.');
                    // hide modal window
                    $mdDialog.hide();
                }else{
                    // show success message about successful updating
                    toast('success', 'Kosmonaut uspěšně upraven.');
                    // hide modal window and return updated cosmonaut data
                    $mdDialog.hide(data);
                }
            }
        }

        /**
         * Create/Update Cosmonaut error callback
         */
        function errorCallback() {
            // hide progress line
            $scope.isLoading = false;
        }

        /**
         * Create/Update Cosmonaut
         * @param cosmonautData
         * @param form
         */
        $scope.submit = function (cosmonautData, form) {
            // show progress line
            $scope.isLoading = true;
            // if mode is 'create'
            if($scope.mode == 'create'){
                // create cosmonaut
                cosmonautsModel.create({cosmonaut: cosmonautData}, successCallback, errorCallback);
            }else{
                // update cosmonaut
                cosmonautsModel.update({cosmonaut: cosmonautData}, successCallback, errorCallback);
            }
        };

        /**
         * Close modal window
         */
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }];