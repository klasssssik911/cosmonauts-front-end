app
    .directive('cHeader', [function() {
        return {
            restrict: 'A',
            templateUrl: 'components/header/header.html',
            controller: 'headerCtrl'
        }
    }])
    .controller('headerCtrl', [
        '$scope', 
        '$element',
        function(
            $scope, 
            $element
        ) {

            $element.addClass('c-header');

            /**
             * Open left menu
             */
            $scope.openLeftMenu = function () {
                $scope.$broadcast('openLeftMenu');
            };

        }
    ]);
